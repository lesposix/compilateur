#Compilateur en C pour le langage turfuresque L.

Compiler avec `make all` pour generer le fichier binaire `compLL` pour utiliser
le compilateur.

Verifier que un dossier `obj/` est present au meme niveau que le Makefile pour abriter les fichiers
.o.

Team Posix
