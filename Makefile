CC = gcc

LIBS = -lm 
CCFLAGS = -Wall -ggdb -g

C_FILES := $(wildcard src/*.c)
OBJ_FILES := $(addprefix obj/,$(notdir $(C_FILES:.c=.o)))

all: compLL

compLL: $(OBJ_FILES)
	$(CC) $(CC_FLAGS) -o $@ $^

obj/%.o: src/%.c
	$(CC) $(CCFLAGS) -c -o $@ $<

.PHONY : clean

clean:
	- rm -f $(OBJ_FILES)
	- rm -f compLL
