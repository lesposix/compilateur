#include "symboles.h"
#include "premiers.h"

void initialise_premiers(void){
	int i,j;

	// Initialiser toutes les cases du tableau à 0
	for(i=0; i <= NB_NON_TERMINAUX; i++)
		for(j=0; j <= NB_TERMINAUX; j++)
			premiers[i][j] = 0;

	init_P_PG(); //PG
	init_P_ODV(); //ODV
	premiers[_listeDecVariables_][ENTIER]=1; //LDV
	init_P_LDVB(); //LDVB
	premiers[_declarationVariable_][ENTIER]=1; //DV
	init_P_OTT(); //OTT
	init_P_LDF(); //LDF
	premiers[_declarationFonction_][ID_FCT]=1; //DF
	premiers[_listeParam_][PARENTHESE_OUVRANTE]=1; //LP
	init_P_OLDV(); //OLDV
	init_P_I(); //I
	premiers[_instructionAffect_][ID_VAR]=1; //IAFF
	premiers[_instructionBloc_][ACCOLADE_OUVRANTE]=1; //IB
	init_P_LI(); //LI
	premiers[_instructionSi_][SI]=1; //ISI
	init_P_OSINON(); //OSINON
	premiers[_instructionTantque_][TANTQUE]=1; //ITQ
    premiers[_instructionPour_][POUR]=1; //POUR
	premiers[_instructionAppel_][ID_FCT]=1; //IAPP
	premiers[_instructionRetour_][RETOUR]=1; //IRET
	premiers[_instructionEcriture_][ECRIRE]=1; //IECR
	premiers[_instructionVide_][POINT_VIRGULE]=1; //IVIDE
	init_P_EXP(); //EXP
	init_P_EXPB(); //EXPB
    init_P_COND();
    init_P_CONDB();
	init_P_CONJ(); //CONJ
	init_P_CONJB(); //CONJB
	init_P_NEG(); //NEG
	init_P_COMP(); //COMP
	init_P_COMPB(); //COMPB
	init_P_E(); //E
	init_P_EB(); //EB
	init_P_T(); //T
	init_P_TB(); //TB
	init_P_F(); //F
	premiers[_var_][ID_VAR]=1; //VAR
	init_P_OIND(); //OIND
	premiers[_appelFct_][ID_FCT]=1; //APPF
	init_P_LEXP(); //LEXP
	init_P_LEXPB(); //LEXPB

}

//PG
void init_P_PG(){ 
	premiers[_programme_][ENTIER] = 1; 
	premiers[_programme_][EPSILON]=1;
	premiers[_programme_][ID_FCT]=1;
}

//ODV
void init_P_ODV(){
	premiers[_optDecVariables_][EPSILON]=1;
	premiers[_optDecVariables_][ENTIER]=1;
}

//LDVB
void init_P_LDVB(){
	premiers[_listeDecVariablesBis_][VIRGULE]=1;
	premiers[_listeDecVariablesBis_][EPSILON]=1;
}

//OTT
void init_P_OTT(){
	premiers[_optTailleTableau_][CROCHET_OUVRANT]=1;
	premiers[_optTailleTableau_][EPSILON]=1;
}

//LDF
void init_P_LDF(){
	premiers[_listeDecFonctions_][EPSILON]=1;
	premiers[_listeDecFonctions_][ID_FCT]=1;	
}

//OLDV
void init_P_OLDV(){
	premiers[_optListeDecVariables_][ENTIER]=1;
	premiers[_optListeDecVariables_][EPSILON]=1;
}

//I
void init_P_I(){
	premiers[_instruction_][ID_VAR]=1;
	premiers[_instruction_][ACCOLADE_OUVRANTE]=1;
	premiers[_instruction_][SI]=1;
	premiers[_instruction_][TANTQUE]=1;
    premiers[_instruction_][POUR]=1;
	premiers[_instruction_][ID_FCT]=1;
	premiers[_instruction_][RETOUR]=1;
	premiers[_instruction_][ECRIRE]=1;
	premiers[_instruction_][POINT_VIRGULE]=1;
}

//LI
void init_P_LI(){
	premiers[_listeInstructions_][ID_VAR]=1;
	premiers[_listeInstructions_][ACCOLADE_OUVRANTE]=1;
	premiers[_listeInstructions_][SI]=1;
	premiers[_listeInstructions_][TANTQUE]=1;
	premiers[_listeInstructions_][ID_FCT]=1;
	premiers[_listeInstructions_][RETOUR]=1;
	premiers[_listeInstructions_][ECRIRE]=1;
	premiers[_listeInstructions_][POINT_VIRGULE]=1;
	premiers[_listeInstructions_][EPSILON]=1;
	premiers[_listeInstructions_][POUR]=1;
}

//OSINON
void init_P_OSINON(){
	premiers[_optSinon_][EPSILON]=1;
	premiers[_optSinon_][SINON]=1;
}

//EXP
void init_P_EXP(){
	premiers[_expression_][NON]=1;
	premiers[_expression_][PARENTHESE_OUVRANTE]=1;
	premiers[_expression_][NOMBRE]=1;
	premiers[_expression_][ID_FCT]=1;
	premiers[_expression_][ID_VAR]=1;
	premiers[_expression_][LIRE]=1;
}

//EXPB
void init_P_EXPB(){
	premiers[_expressionBis_][INTERROGATION]=1;
	premiers[_expressionBis_][EPSILON]=1;
}

//CONJ
void init_P_CONJ(){
	premiers[_conjonction_][NON]=1;
	premiers[_conjonction_][PARENTHESE_OUVRANTE]=1;
	premiers[_conjonction_][NOMBRE]=1;
	premiers[_conjonction_][ID_FCT]=1;
	premiers[_conjonction_][ID_VAR]=1;
	premiers[_conjonction_][LIRE]=1;
}

//CONJB
void init_P_CONJB(){
	premiers[_conjonctionBis_][ET]=1;
	premiers[_conjonctionBis_][EPSILON]=1;
}

//NEG
void init_P_NEG(){
	premiers[_negation_][NON]=1;
	premiers[_negation_][PARENTHESE_OUVRANTE]=1;
	premiers[_negation_][NOMBRE]=1;
	premiers[_negation_][ID_FCT]=1;
	premiers[_negation_][ID_VAR]=1;
	premiers[_negation_][LIRE]=1;
}

//COMP
void init_P_COMP(){
	premiers[_comparaison_][PARENTHESE_OUVRANTE]=1;
	premiers[_comparaison_][NOMBRE]=1;
	premiers[_comparaison_][ID_FCT]=1;
	premiers[_comparaison_][ID_VAR]=1;
	premiers[_comparaison_][LIRE]=1;
}

//COMPB
void init_P_COMPB(){
	premiers[_comparaisonBis_][EPSILON]=1;
	premiers[_comparaisonBis_][INFERIEUR]=1;
	premiers[_comparaisonBis_][EGAL]=1;
}

//E
void init_P_E(){
	premiers[_expArith_][PARENTHESE_OUVRANTE]=1;
	premiers[_expArith_][NOMBRE]=1;
	premiers[_expArith_][ID_FCT]=1;
	premiers[_expArith_][ID_VAR]=1;
	premiers[_expArith_][LIRE]=1;
}

//EB
void init_P_EB(){
	premiers[_expArithBis_][PLUS]=1;
	premiers[_expArithBis_][MOINS]=1;
	premiers[_expArithBis_][EPSILON]=1;
}

//T
void init_P_T(){
	premiers[_terme_][PARENTHESE_OUVRANTE]=1;
	premiers[_terme_][NOMBRE]=1;
	premiers[_terme_][ID_FCT]=1;
	premiers[_terme_][ID_VAR]=1;
	premiers[_terme_][LIRE]=1;
}

//TB
void init_P_TB(){
	premiers[_termeBis_][FOIS]=1;
	premiers[_termeBis_][DIVISE]=1;
	premiers[_termeBis_][EPSILON]=1;
}

//F
void init_P_F(){
	premiers[_facteur_][PARENTHESE_OUVRANTE]=1;
	premiers[_facteur_][NOMBRE]=1;
	premiers[_facteur_][ID_FCT]=1;
	premiers[_facteur_][ID_VAR]=1;
	premiers[_facteur_][LIRE]=1;
}

//OIND
void init_P_OIND(){
	premiers[_optIndice_][CROCHET_OUVRANT]=1;
	premiers[_optIndice_][EPSILON]=1;
}

//LEXP
void init_P_LEXP(){
	premiers[_listeExpressions_][EPSILON]=1;
	premiers[_listeExpressions_][NON]=1;
	premiers[_listeExpressions_][PARENTHESE_OUVRANTE]=1;
	premiers[_listeExpressions_][NOMBRE]=1;
	premiers[_listeExpressions_][ID_FCT]=1;
	premiers[_listeExpressions_][ID_VAR]=1;
	premiers[_listeExpressions_][LIRE]=1;
}

//LEXPB
void init_P_LEXPB(){
	premiers[_listeExpressionsBis_][VIRGULE]=1;
	premiers[_listeExpressionsBis_][EPSILON]=1;
}

void init_P_COND() {
	premiers[_condition_][NON]=1;
	premiers[_condition_][PARENTHESE_OUVRANTE]=1;
	premiers[_condition_][NOMBRE]=1;
	premiers[_condition_][ID_FCT]=1;
	premiers[_condition_][ID_VAR]=1;
	premiers[_condition_][LIRE]=1;
}

void init_P_CONDB(){
	premiers[_conditionBis_][EPSILON]=1;
	premiers[_conditionBis_][OU]=1;
}

int est_premier(int terminal, int non_terminal)
{
  return premiers[non_terminal][terminal];
}

