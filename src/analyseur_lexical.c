#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "symboles.h"
#include "analyseur_lexical.h"
#include "util.h"

#define YYTEXT_MAX 100
#define is_num(c)(('0' <= (c)) && ((c) <= '9'))
#define is_maj(c)(('A' <= (c)) && ((c) <= 'Z'))
#define is_min(c)(('a' <= (c)) && ((c) <= 'z'))
#define is_alpha(c)(is_maj(c) || is_min(c) || (c) == '_' || (c) == '$')
#define is_alphanum(c)(is_num((c)) || is_alpha((c)))

extern FILE *yyin;

char *tableMotsClefs[] =
{
    "si", "alors", "sinon", "tantque", "faire", "entier", "retour", "lire", "ecrire", "pour"
};

int codeMotsClefs[] =
{
    SI, ALORS, SINON, TANTQUE, FAIRE, ENTIER, RETOUR, LIRE, ECRIRE, POUR
};

int sizeMotsClefs = 10;

char tableSymbolesSimples[] =
{
    ';', ',', '+', '-', '*', '/', '(', ')', '[', ']', '{', '}', '=', '<', '&', '|', '!','?',':'
};

int codeSymbolesSimples[] =
{
    POINT_VIRGULE, VIRGULE, PLUS, MOINS, FOIS, DIVISE, PARENTHESE_OUVRANTE, PARENTHESE_FERMANTE, CROCHET_OUVRANT,
    CROCHET_FERMANT, ACCOLADE_OUVRANTE, ACCOLADE_FERMANTE, EGAL, INFERIEUR, ET, OU, NON, INTERROGATION, DEUXPOINTS
};

int sizeSymbolesSimples = 19;

char yytext[YYTEXT_MAX];
int yyleng;
int nbMotsClefs = 10;
/* Compter les lignes pour afficher les messages d'erreur avec numero ligne */
int nb_ligne = 1;

/*******************************************************************************
 * Fonction qui ignore les espaces et commentaires.
 * Renvoie -1 si arrivé à la fin du fichier, 0 si tout va bien
 ******************************************************************************/
int mangeEspaces()
{
    char c = fgetc(yyin);
    int comment = 0;
    while (comment || (c == ' ') || (c == '\n') || (c == '\t') || (c == '#'))
    {
        if (c == '#')
        {
            comment = 1;
        }
        if (c == '\n')
        {
            nb_ligne++;
            comment = 0;
        }
        c = fgetc(yyin);
    }
    if (feof(yyin))
    {
        return -1;
    }
    ungetc(c, yyin);
    return 0;
}

/*******************************************************************************
 * Lit un caractère et le stocke dans le buffer yytext
 ******************************************************************************/
char lireCar(void)
{
    yytext[yyleng++] = fgetc(yyin);
    yytext[yyleng] = '\0';
    return yytext[yyleng - 1];
}

/*******************************************************************************
 * Remet le dernier caractère lu au buffer clavier et enlève du buffer yytext
 ******************************************************************************/
void delireCar()
{
    char c;
    c = yytext[yyleng - 1];
    yytext[--yyleng] = '\0';
    ungetc(c, yyin);
}

/*******************************************************************************
 * Fonction principale de l'analyseur lexical, lit les caractères de yyin et
 * renvoie les tokens sous forme d'entier. Le code de chaque unité est défini
 * dans symboles.h sinon (mot clé, idententifiant, etc.). Pour les tokens de
 * type ID_FCT, ID_VAR et NOMBRE la valeur du token est dans yytext, visible
 * dans l'analyseur syntaxique.
 ******************************************************************************/
int yylex(void)
{
    char c;
    int i;
    yytext[yyleng = 0] = '\0';
    mangeEspaces();

    c = lireCar();

    if(c==EOF){
        delireCar();
        return FIN;
    }
    i = is_symbole_simple(c);
    if (i != -1)   // si c'est un symbole simple
    {
        return i;
    }
    else if (is_num(c))   //si c'est un chiffre, debut d'un nombre
    {
        while (is_num(c))
        {
            c = lireCar();
        }
        delireCar();
        return NOMBRE;
    }
    else if (c == '$')   //si commence par un $, c'est une variable
    {
        while (is_alphanum(c))
        {
            c = lireCar();
        }
        delireCar();
        return ID_VAR;
    }
    else   //mot clef ou fonction
    {
        while (is_alphanum(c))
        {
            c = lireCar();
        }
        delireCar();
        i = is_mot_clefs();
        if (i != -1) //si c'est un mot clef
            return i;

        return ID_FCT;
    }
}

//Retourne le code si le char est un symbole simple sinon -1
int is_symbole_simple(char c)
{
    int i = 0;
    while (i < sizeSymbolesSimples)
    {
        if (tableSymbolesSimples[i] == c)
            return codeSymbolesSimples[i];
        ++i;
    }
    return -1;
}

/*
 * Retourne le code du mot clefs correspondant ou renvoie -1 si le mot dans le buffer n'est pas un mot clef
 */
int is_mot_clefs()
{
    int j;
    for (j = 0; j < sizeMotsClefs; ++j)   // on regarde si le mot obtnue correspond a un mot clef
    {
        if (strcmp(tableMotsClefs[j], yytext) == 0)
            return codeMotsClefs[j];
    }
    return -1;
}

/*******************************************************************************
 * Fonction auxiliaire appelée par l'analyseur syntaxique tout simplement pour
 * afficher des messages d'erreur et l'arbre XML
 ******************************************************************************/
void nom_token(int token, char *nom, char *valeur)
{
    int i;

    strcpy(nom, "symbole");
    if (token == POINT_VIRGULE) strcpy(valeur, "POINT_VIRGULE");
    else if (token == PLUS) strcpy(valeur, "PLUS");
    else if (token == MOINS) strcpy(valeur, "MOINS");
    else if (token == FOIS) strcpy(valeur, "FOIS");
    else if (token == DIVISE) strcpy(valeur, "DIVISE");
    else if (token == PARENTHESE_OUVRANTE) strcpy(valeur, "PARENTHESE_OUVRANTE");
    else if (token == PARENTHESE_FERMANTE) strcpy(valeur, "PARENTHESE_FERMANTE");
    else if (token == CROCHET_OUVRANT) strcpy(valeur, "CROCHET_OUVRANT");
    else if (token == CROCHET_FERMANT) strcpy(valeur, "CROCHET_FERMANT");
    else if (token == ACCOLADE_OUVRANTE) strcpy(valeur, "ACCOLADE_OUVRANTE");
    else if (token == ACCOLADE_FERMANTE) strcpy(valeur, "ACCOLADE_FERMANTE");
    else if (token == EGAL) strcpy(valeur, "EGAL");
    else if (token == INFERIEUR) strcpy(valeur, "INFERIEUR");
    else if (token == ET) strcpy(valeur, "ET");
    else if (token == OU) strcpy(valeur, "OU");
    else if (token == NON) strcpy(valeur, "NON");
    else if (token == FIN) strcpy(valeur, "FIN");
    else if (token == VIRGULE) strcpy(valeur, "VIRGULE");
    else if(token == DEUXPOINTS) strcpy(valeur, "DEUXPOINTS");
    else if(token == INTERROGATION) strcpy(valeur, "INTERROGATION");

    else if (token == ID_VAR)
    {
        strcpy(nom, "id_variable");
        strcpy(valeur, yytext);
    }
    else if (token == ID_FCT)
    {
        strcpy(nom, "id_fonction");
        strcpy(valeur, yytext);
    }
    else if (token == NOMBRE)
    {
        strcpy(nom, "nombre");
        strcpy(valeur, yytext);
    }
    else
    {
        strcpy(nom, "mot_clef");
        for (i = 0; i < nbMotsClefs; i++)
        {
            if (token == codeMotsClefs[i])
            {
                strcpy(valeur, tableMotsClefs[i]);
                break;
            }
        }
    }
}

void affiche_xml_valeur(int token){
    char nom[100];
    char valeur[100];
    nom_token(token,nom,valeur);
    affiche_element(nom,valeur,AFFICHE_XML);
}

/*******************************************************************************
 * Fonction auxiliaire appelée par le compilo en mode -l, pour tester l'analyseur
 * lexical et, étant donné un programme en entrée, afficher la liste des tokens.
 ******************************************************************************/

void test_yylex_internal(FILE *yyin)
{
    int uniteCourante;
    char nom[100];
    char valeur[100];
    do
    {
        uniteCourante = yylex();
        nom_token(uniteCourante, nom, valeur);
        printf("%s\t%s\t%s\n", yytext, nom, valeur);
    }
    while (uniteCourante != FIN);
}
