#ifndef __PREMIERS__
#define __PREMIERS__

#include"symboles.h"

int premiers[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];

void initialise_premiers(void);

void init_P_PG();
void init_P_ODV();
void init_P_LDVB();
void init_P_OTT();
void init_P_LDF();
void init_P_OLDV();
void init_P_I();
void init_P_LI();
void init_P_OSINON();
void init_P_EXP();
void init_P_EXPB();
void init_P_COND();
void init_P_CONDB();
void init_P_CONJ();
void init_P_CONJB();
void init_P_NEG();
void init_P_COMP();
void init_P_COMPB();
void init_P_E();
void init_P_EB();
void init_P_T();
void init_P_TB();
void init_P_F();
void init_P_OIND();
void init_P_LEXP();
void init_P_LEXPB();

int est_premier(int non_terminal, int terminal);

#endif
