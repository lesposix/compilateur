#ifndef __UTIL__
#define __UTIL__

#include <stdlib.h>

int AFFICHE_XML;
int AFFICHE_AA;
int AFFICHE_DICO;
int AFFICHE_MIPS;

char *duplique_chaine(char *s);
void erreur(char *message);
void erreur_1s(char *message, char *s);
void affiche_balise_ouvrante(const char *fct_, int trace_xml);
void affiche_balise_fermante(const char *fct_, int trace_xml);
void affiche_element(char *fct_, char *texte_, int trace_xml);
void affiche_texte(char *texte_, int trace_xml);

#endif

