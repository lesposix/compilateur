#ifndef __SUIVANTS__
#define __SUIVANTS__

#include"symboles.h"

int suivants[NB_NON_TERMINAUX+1][NB_TERMINAUX+1];

void initialise_suivants(void);
int est_suivant(int non_terminal, int terminal);

void init_S_PG();
void init_S_ODV();
void init_S_LDV();
void init_S_LDVB();
void init_S_DV();
void init_S_OTT();
void init_S_LDF();
void init_S_DF();
void init_S_LP();
void init_S_OLDV();
void init_S_I();
void init_S_IAFF();
void init_S_IB();
void init_S_LI();
void init_S_ISI();
void init_S_OSINON();
void init_S_ITQ();
void init_S_IPOUR();
void init_S_IAPP();
void init_S_IRET();
void init_S_IECR();
void init_S_IVIDE();
void init_S_EXP();
void init_S_EXPB();
void init_S_COND();
void init_S_CONDB();
void init_S_CONJ();
void init_S_CONJB();
void init_S_NEG();
void init_S_COMP();
void init_S_COMPB();
void init_S_E();
void init_S_EB();
void init_S_T();
void init_S_TB();
void init_S_F();
void init_S_VAR();
void init_S_OIND();
void init_S_APPF();
void init_S_LEXP();
void init_S_LEXPB();

#endif
