#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "analyseur_syntaxique.h"
#include "analyseur_lexical.h"
#include "util.h"

char yytext[100];
FILE *yyin;

void syntaxe(){
    AFFICHE_XML=1;
    AFFICHE_AA=0;
    AFFICHE_DICO=0;
    AFFICHE_MIPS=0;
    analyser_syntaxe(yyin);
}

void arbre_abstrait(){
    AFFICHE_XML=0;
    AFFICHE_AA=1;
    AFFICHE_DICO=0;
    AFFICHE_MIPS=0;
    analyser_syntaxe(yyin);
}

void table_symboles(){
    AFFICHE_XML=0;
    AFFICHE_AA=0;
    AFFICHE_DICO=1;
    AFFICHE_MIPS=0;
    analyser_syntaxe(yyin);
}

void mips(){
    AFFICHE_XML=0;
    AFFICHE_AA=0;
    AFFICHE_DICO=0;
    AFFICHE_MIPS=1;
    analyser_syntaxe(yyin);
}

void defaut(){
    AFFICHE_XML=0;
    AFFICHE_AA=0;
    AFFICHE_DICO=0;
    AFFICHE_MIPS=0;
    analyser_syntaxe(yyin);

}

int main(int argc, char **argv) {  
    if(argc<2){
        printf("erreur arguments");
        exit(EXIT_FAILURE);
    }
    if(argc == 2){
	yyin = fopen(argv[1], "r");
	    if(yyin == NULL){
		fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
		exit(1);
	    }
        defaut();
    }
    else{
	yyin = fopen(argv[2], "r");
	    if(yyin == NULL){
		fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
		exit(1);
	    }
	    if(strcmp(argv[1],"-l")==0)
		test_yylex_internal(yyin);
	    else if(strcmp(argv[1],"-s")==0)
		syntaxe();
	    else if(strcmp(argv[1],"-a")==0)
		arbre_abstrait();
	    else if(strcmp(argv[1],"-t")==0)
		table_symboles();
	    else if(strcmp(argv[1],"-m")==0)
		mips();
    }	
    return 0;
}

