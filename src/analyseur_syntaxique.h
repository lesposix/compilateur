#ifndef ANALYSEUR_SYNTAXIQUE_H
#define ANALYSEUR_SYNTAXIQUE_H

#include "stdio.h"
#include "syntabs.h"

int uniteCourante;
extern char* SYNTAX_ERROR_MSG;

void analyser_syntaxe(FILE *yyin);
void affiche_et_next();
n_prog *PG();
n_l_dec *ODV();
n_l_dec *LDV();
n_l_dec *LDVB(n_l_dec *herite);
n_dec *DV();
n_exp *OTT();
n_l_dec *LDF();
n_dec *DF();
n_l_dec *LP();
n_l_dec *OLDV();
n_instr *I();
n_instr *IAFF();
n_instr *IB();
n_l_instr *LI();
n_instr *ISI();
n_instr *OSINON();
n_instr *ITQ();
n_instr *IPOUR();
n_instr *IAPP();
n_instr *IRET();
n_instr *IECR();
n_instr *IVIDE();
n_exp *EXP();
n_exp *EXPB(n_exp *herite);
n_exp *COND();
n_exp *CONDB(n_exp *herite);
n_exp *CONJ();
n_exp *CONJB(n_exp *herite);
n_exp *NEG(n_exp *herite);
n_exp *COMP();
n_exp *COMPB(n_exp *herite);
n_exp *E();
n_exp *EB(n_exp *herite);
n_exp *T();
n_exp *TB(n_exp *herite);
n_exp *F();
n_var *VAR();
n_exp *OIND();
n_appel *APPF();
n_l_exp *LEXP();
n_l_exp *LEXPB(n_l_exp *herite);

#endif
