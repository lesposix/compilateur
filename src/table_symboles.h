#ifndef __TABLE_SYMBOLES__
#define __TABLE_SYMBOLES__

#include "syntabs.h"

void affiche_table_symboles(n_prog *n);
void empile(char *registre);
void depile(char *registre);
int newTag();

#endif


