#include <stdio.h>
#include <string.h>
#include "syntabs.h"
#include "util.h"
#include "dico.h"
#include "table_symboles.h"

void affiche_table_symboles(n_prog *n);

void table_l_instr(n_l_instr *n);

void table_instr(n_instr *n);

int table_instr_si(n_instr *n);

int table_instr_tantque(n_instr *n);

void table_instr_faire(n_instr *n);

int table_instr_pour(n_instr *n);

void table_instr_affect(n_instr *n);

void table_instr_appel(n_instr *n);

void table_instr_retour(n_instr *n);

void table_instr_ecrire(n_instr *n);

void table_l_exp(n_l_exp *n);

void table_exp(n_exp *n);

void table_varExp(n_exp *n);

void table_opExp(n_exp *n);

void table_intExp(n_exp *n);

void table_lireExp(n_exp *n);

void table_appelExp(n_exp *n);

void table_condExp(n_exp *n);

void table_l_dec(n_l_dec *n);

void table_dec(n_dec *n);

void table_foncDec(n_dec *n);

void table_varDec(n_dec *n);

void table_tabDec(n_dec *n);

void table_var(n_var *n);

void table_var_simple(n_var *n);

void table_var_indicee(n_var *n);

void table_appel(n_appel *n);

int size_n_l_dec(n_l_dec *n);

int size_n_l_exp(n_l_exp *n);

int contexte = C_VARIABLE_GLOBALE;
int adresseLocaleCourante;
int adresseArgumentCourant;
int adresseGlobale;
int cptInstr = -1;
int nbParam;
int nb_var_loc;

struct returnFonc {
    char *nomFonc[64];
    int valReturn[64];
    int cpt;
};

struct returnFonc returnFonc;

/*-------------------------------------------------------------------------*/

void affiche_table_symboles(n_prog *n) {
    affiche_texte("\t.data", AFFICHE_MIPS);
    table_l_dec(n->variables);
    if (AFFICHE_MIPS) {
        printf("\n");
        printf("\t.text\n");
        printf("__start:\n");
        printf("\tjal\tmain\n");
        printf("\tli\t$v0, 10\n");
        printf("\tsyscall\t#stoppe l'execution\n");
    }
    table_l_dec(n->fonctions);
    if (rechercheExecutable("main") == -1) {
        erreur_1s("Erreur dans la table des symboles",
                  "Fonction main non trouvée");
        exit(EXIT_FAILURE);
    }
    if (dico.tab[rechercheExecutable("main")].complement != 0) {
        erreur_1s(
                "Erreur dans la table des symboles",
                "Fonction main non valide, la fonction main ne doit pas avoir "
                        "d'arguments");
        exit(EXIT_FAILURE);
    }
}

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

void table_l_instr(n_l_instr *n) {
    if (n) {
        table_instr(n->tete);
        table_l_instr(n->queue);
    }
}

/*-------------------------------------------------------------------------*/

void table_instr(n_instr *n) {
    if (n) {
        if (n->type == blocInst)
            table_l_instr(n->u.liste);
        else if (n->type == affecteInst)
            table_instr_affect(n);
        else if (n->type == siInst) {
            int suite = table_instr_si(n);
            if (AFFICHE_MIPS) printf("e%d:\n", suite);
        } else if (n->type == tantqueInst) {
            int suite = table_instr_tantque(n);
            if (AFFICHE_MIPS) printf("e%d:\n", suite);
        } else if (n->type == pourInst) {
            int suite = table_instr_pour(n);
            if (AFFICHE_MIPS) printf("e%d:\n", suite);
        } else if (n->type == faireInst)
            table_instr_faire(n);
        else if (n->type == appelInst)
            table_instr_appel(n);
        else if (n->type == retourInst)
            table_instr_retour(n);
        else if (n->type == ecrireInst)
            table_instr_ecrire(n);
    }
}

/*-------------------------------------------------------------------------*/

int table_instr_si(n_instr *n) {
    int si_vrai, si_faux, si_suite;
    si_vrai = newTag();
    si_suite = newTag();
    table_exp(n->u.si_.test);
    if (AFFICHE_MIPS) {
        // empile("$t2");
        depile("$t0");
        if (n->u.si_.sinon) {
            si_faux = newTag();
            printf("\tbeq\t$t0, $zero, e%d\t# branchement si_sinon\n",
                   si_faux);  // beq $t0,$zero,faux
        } else
            printf("\tbeq\t$t0, $zero, e%d\t# branchement si_suite\n",
                   si_suite);  // beq $t0,$zero,suite
    }
    table_instr(n->u.si_.alors);
    if (n->u.si_.sinon) {
        if (AFFICHE_MIPS) {
            printf("\tj\te%d#si_suite\n", si_suite);  // j suite
            printf("e%d:#si_faux\n", si_faux);        // faux
        }
    }
    table_instr(n->u.si_.sinon);
    return si_suite;
}

/*-------------------------------------------------------------------------*/

int table_instr_tantque(n_instr *n) {
    int tq_test, tq_suite;
    tq_test = newTag();
    tq_suite = newTag();
    if (AFFICHE_MIPS) {  // test de la condition de boucle
        printf("e%d:#tq_test\n", tq_test);
    }
    table_exp(n->u.tantque_.test);
    if (AFFICHE_MIPS) {  // instructions de la boucle
        depile("$t0");
        printf("\tbeq\t$t0, $zero, e%d\t#tq_suite\n",
               tq_suite);  // beq $t0,$zero,i_suite
    }
    table_instr(n->u.tantque_.faire);
    if (AFFICHE_MIPS) printf("\tj\te%d\t#tq_test\n", tq_test);  // j test
    return tq_suite;
}

/*-------------------------------------------------------------------------*/

void table_instr_faire(n_instr *n) {
    table_instr(n->u.faire_.faire);
    table_exp(n->u.faire_.test);
}

/*-------------------------------------------------------------------------*/

int table_instr_pour(n_instr *n) {
    int pour_test, pour_suite;
    pour_test = newTag();
    pour_suite = newTag();
    table_instr(n->u.pour_.init);

    if (AFFICHE_MIPS) printf("e%d: \n", pour_test);
    table_exp(n->u.pour_.test);

    if (AFFICHE_MIPS) {
        depile("$t0");
        printf("\tbeq $t0, $0, e%d\n", pour_suite);
    }

    table_instr(n->u.pour_.faire);
    table_instr(n->u.pour_.incr);

    if (AFFICHE_MIPS) printf("\tj e%d\n", pour_test);
    return pour_suite;
}

/*-------------------------------------------------------------------------*/

void table_instr_affect(n_instr *n) {
    table_exp(n->u.affecte_.exp);
    table_var(n->u.affecte_.var);
    if (AFFICHE_MIPS) {
        int i;
        if ((i = rechercheExecutable(n->u.affecte_.var->nom)) != -1) {
            desc_identif *var = &dico.tab[i];
            if (var->type != T_TABLEAU_ENTIER) {
                depile("$t1");
                if (var->classe == C_VARIABLE_GLOBALE)
                    printf("\tsw\t$t1, v%s\n",
                           n->u.affecte_.var->nom);  // sw $t1,nomVar
                else if (var->classe == C_VARIABLE_LOCALE)
                    printf("\tsw\t$t1, %d($fp)\t#var locale\n",
                           (-8 - var->adresse));
                else
                    printf("\tsw\t$t1, %d($fp)\t#argument\n",
                           (4 * nbParam) - var->adresse);

            } else {
                depile("$t0");
                printf("\tadd\t$t0, $t0, $t0\n");
                printf("\tadd\t$t0, $t0, $t0\n");
                depile("$t1");
                printf("\tsw\t$t1, v%s($t0)\n", n->u.affecte_.var->nom);
            }
        }
    }
}

/*-------------------------------------------------------------------------*/

void table_instr_appel(n_instr *n) { table_appel(n->u.appel); }

/*-------------------------------------------------------------------------*/

void table_appel(n_appel *n) {
    int nb_param, i;
    if ((i = rechercheExecutable(n->fonction)) != -1) {
        desc_identif *fonc = &dico.tab[i];
        nb_param = size_n_l_exp(n->args);
        if (nb_param != fonc->complement) {
            erreur_1s("Erreur dans la table des symboles",
                      "Nombre arguments invalides");
            printf("%s \n", n->fonction);
            exit(EXIT_FAILURE);
        }
        if (AFFICHE_MIPS)
            printf("\tsubi $sp, $sp, 4\t#allocation de la valeur de retour\n");
        table_l_exp(n->args);
        if (AFFICHE_MIPS) {
            printf("\tjal %s\n", n->fonction);
            printf("\taddi $sp, $sp, %d\t#depile les arguments\n",
                   nb_param * 4);
            int i;
            for (i = 0; i < returnFonc.cpt; ++i) {
                if (strcmp(n->fonction, returnFonc.nomFonc[i]) == 0) {
                    if (returnFonc.valReturn[i] != 1)
                        printf(
                                "\taddi\t$sp, $sp, 4\t#desalloc valeur retour\n");
                }
            }
        }
        return;
    }

    printf("%s \n", n->fonction);
    erreur_1s("Erreur dans la table des symboles", "Fonction non déclarée");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/

void table_instr_retour(n_instr *n) {
    table_exp(n->u.retour_.expression);
    if (AFFICHE_MIPS) {
        depile("$t0");
        printf("\tsw $t0, %d($fp)\n", 4 * (nbParam + 1));
        if (nb_var_loc != 0)
            printf("\taddi $sp, $sp, %d\t#desaloc var locales (retour)\n",
                   nb_var_loc * 4);
        depile("$ra");
        depile("$fp");
        printf("\tjr $ra\n");
        returnFonc.valReturn[returnFonc.cpt] = 1;
    }
}

/*-------------------------------------------------------------------------*/

void table_instr_ecrire(n_instr *n) {
    table_exp(n->u.ecrire_.expression);
    if (AFFICHE_MIPS) {
        depile("$a0");
        affiche_texte("\tli\t$v0, 1", AFFICHE_MIPS);             // li $v0,1
        affiche_texte("\tsyscall\t#Ecrire", AFFICHE_MIPS);       // syscall
        affiche_texte("\tli\t$a0, \'\\n\'", AFFICHE_MIPS);       // li $a0, '\n'
        affiche_texte("\tli\t$v0, 11", AFFICHE_MIPS);            // li $v0, 11
        affiche_texte("\tsyscall\t#ecrire char", AFFICHE_MIPS);  // syscall
    }
}

/*-------------------------------------------------------------------------*/

void table_l_exp(n_l_exp *n) {
    if (n) {
        table_exp(n->tete);
        table_l_exp(n->queue);
    }
}

/*-------------------------------------------------------------------------*/

void table_exp(n_exp *n) {
    if (n->type == varExp)
        table_varExp(n);
    else if (n->type == opExp) {
        if (n->u.opExp_.op3 != NULL)
            table_condExp(n);
        else
            table_opExp(n);
    } else if (n->type == intExp)
        table_intExp(n);
    else if (n->type == appelExp)
        table_appelExp(n);
    else if (n->type == lireExp)
        table_lireExp(n);
}

/*-------------------------------------------------------------------------*/

void table_varExp(n_exp *n) {
    table_var(n->u.var);
    if (AFFICHE_MIPS) {
        int i;
        if ((i = rechercheExecutable(n->u.var->nom)) != -1) {
            desc_identif *var = &dico.tab[i];
            if (var->type != T_TABLEAU_ENTIER) {
                if (var->classe == C_VARIABLE_GLOBALE)
                    printf("\tlw\t$t1, v%s\t# var exp\n",
                           n->u.var->nom);  // lw $t1,nomVar
                else if (var->classe == C_VARIABLE_LOCALE)
                    printf("\tlw\t$t1, %d($fp)\t#var locale %s\n",
                           (-8 - var->adresse), n->u.var->nom);
                else
                    printf("\tlw\t$t1, %d($fp)\t#argument %s\n",
                           (4 * nbParam) - var->adresse, n->u.var->nom);
            } else {
                depile("$t0");
                printf("\tadd\t$t0, $t0, $t0\n");
                printf("\tadd\t$t0, $t0, $t0\n");
                printf("\tlw\t$t1, v%s($t0)\n", n->u.var->nom);
            }
            empile("$t1");
        }
    }
}

/*-------------------------------------------------------------------------*/
void table_opExp(n_exp *n) {
    if (n->u.opExp_.op1 != NULL) {
        if (AFFICHE_MIPS) {
            table_exp(n->u.opExp_.op1);
            if(n->u.opExp_.op == non) {
                depile("$t0");
                printf("\tnot $t1, $t0\n");
                empile("$t1");
            }
        }
    }
    if (n->u.opExp_.op2 != NULL) {
        if (AFFICHE_MIPS) {
            int tag;
            switch (n->u.opExp_.op) {
                case fois:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tmult $t0, $t1\n");
                    printf("\tmflo $t2\n");
                    empile("$t2");
                    break;
                case plus:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tadd $t2, $t0, $t1\n");
                    empile("$t2");
                    break;
                case moins:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tsub $t2, $t0, $t1\n");
                    empile("$t2");
                    break;
                case divise:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tdiv $t0, $t1\n");
                    printf("\tmflo $t2\n");
                    empile("$t2");
                    break;
                case modulo:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tdiv $t0, $t1\n");
                    printf("\tmfhi $t2\n");
                    empile("$t2");
                    break;
                case ou:
                    depile("$t0");
                    tag = newTag();
                    empile("$t0");
                    printf(
                            "\tbne $t0, $0, cc%d\t#si ce que j'ai à gauche est "
                                    "vrai "
                                    "je n'evalue pas a droite\n",
                            tag);
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tor $t2, $t0, $t1\n");
                    empile("$t2");
                    printf("cc%d:#cc ou\n", tag);
                    break;
                case et:
                    depile("$t0");
                    tag = newTag();
                    empile("$t0");
                    printf(
                            "\tbeq $t0, $0, cc%d\t#si ce que j'ai à gauche est "
                                    "faux "
                                    "je n'evalue pas a droite\n",
                            tag);
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    printf("\tand $t2, $t0, $t1\n");
                    empile("$t2");
                    printf("cc%d:#cc ou\n", tag);
                    break;
                case egal:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli\t$t2, -1\t#test égalité\n");
                    printf("\tbeq $t0, $t1, e%d\n", tag);  // beq $t0, $t1, tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                case diff:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli $t2, -1\t#test différence\n");
                    printf("\tbne $t0, $t1, e%d\n", tag);  // bne $t0,$t1,tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                case inf:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli $t2, -1\t#test inférieur\n");
                    printf("\tblt $t0, $t1, e%d\n", tag);  // blt $t0, $t1, tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                case sup:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli $t2, -1\t#test supérieur\n");
                    printf("\tbgt $t0, $t1, e%d\n", tag);  // bgt $t0,$t1,tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                case infeg:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli $t2, -1\t#test inférieur ou egal\n");
                    printf("\tble $t0, $t1, e%d\n", tag);  // ble $t0,$t1,tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                case supeg:
                    table_exp(n->u.opExp_.op2);
                    depile("$t1");
                    depile("$t0");
                    tag = newTag();
                    printf("\tli $t2, -1\t#test supérieur ou egal\n");
                    printf("\tbge $t0, $t1, e%d\n", tag);  // bge $t0,$t1,tag
                    printf("\tli $t2, 0\n");
                    printf("e%d:\n", tag);
                    empile("$t2");
                    break;
                default:
                    break;
            }
        }
    }
}

/*-------------------------------------------------------------------------*/

void table_intExp(n_exp *n) {
    if (AFFICHE_MIPS) {
        printf("\tli\t$t0, %d\n", n->u.entier);  // li $t0,int
        empile("$t0");
    }
}

/*-------------------------------------------------------------------------*/
void table_lireExp(n_exp *n) {
    if (AFFICHE_MIPS) {
        printf("\tli\t$v0, 5\n");
        printf("\tsyscall\t#lire\n");
        empile("$v0");
    }
}

/*-------------------------------------------------------------------------*/

void table_appelExp(n_exp *n) { table_appel(n->u.appel); }

/*-------------------------------------------------------------------------*/

void table_condExp(n_exp *n) {
    int cond_suite, cond_faux;
    cond_suite = newTag();
    cond_faux = newTag();
    table_exp(n->u.opExp_.op1);
    if (AFFICHE_MIPS) {
        depile("$t0");
        printf("\tbeq $t0, $zero, cond_f%d\n", cond_faux);
    }
    table_exp(n->u.opExp_.op2);
    if (AFFICHE_MIPS) {
        printf("\tj\tcond_s%d\n", cond_suite);
        printf("cond_f%d:\n", cond_faux);
    }
    table_exp(n->u.opExp_.op3);
    if (AFFICHE_MIPS) printf("cond_s%d:\n", cond_suite);
}

/*-------------------------------------------------------------------------*/

void table_l_dec(n_l_dec *n) {
    if (n) {
        table_dec(n->tete);
        table_l_dec(n->queue);
    }
}

/*-------------------------------------------------------------------------*/

void table_dec(n_dec *n) {
    if (n) {
        if (n->type == foncDec) {
            table_foncDec(n);
        } else if (n->type == varDec) {
            table_varDec(n);
        } else if (n->type == tabDec) {
            table_tabDec(n);
        }
    }
}

/*-------------------------------------------------------------------------*/

void table_foncDec(n_dec *n) {
    int nb_param;
    if (rechercheDeclarative(n->nom) == -1) {
        nb_param = size_n_l_dec(n->u.foncDec_.param);
        nbParam = nb_param;
        ajouteIdentificateur(n->nom, C_VARIABLE_GLOBALE, T_FONCTION, 0,
                             nb_param);
        entreeFonction();
        if (AFFICHE_MIPS) {
            printf("%s:\n", n->nom);
            empile("$fp");
            printf("\tmove\t$fp, $sp\n");
            empile("$ra");
        }
        contexte = C_ARGUMENT;
        table_l_dec(n->u.foncDec_.param);
        contexte = C_VARIABLE_LOCALE;
        table_l_dec(n->u.foncDec_.variables);
        if (AFFICHE_MIPS) {
            nb_var_loc = size_n_l_dec(n->u.foncDec_.variables);
            if (nb_var_loc != 0)
                printf("\tsubi\t$sp, $sp, %d\t# allocation variables locales\n",
                       nb_var_loc * 4);
        }
        table_instr(n->u.foncDec_.corps);
        if (AFFICHE_MIPS) {
            if (nb_var_loc != 0)
                printf("\taddi\t$sp, $sp, %d#desaloc var locales\n",
                       nb_var_loc * 4);
            depile("$ra");
            depile("$fp");
            printf("\tjr\t$ra\n");
        }
        if (AFFICHE_DICO) affiche_dico();  // aficher dico
        sortieFonction();
        returnFonc.nomFonc[returnFonc.cpt] = n->nom;
        returnFonc.cpt += 1;

        return;
    }

    printf("%s \n", n->nom);
    erreur_1s("Erreur dans la table des symboles", "Fonction déja déclarée");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/

void table_varDec(n_dec *n) {
    if (rechercheDeclarative(n->nom) == -1) {
        switch (contexte) {
            case C_VARIABLE_GLOBALE:
                ajouteIdentificateur(n->nom, contexte, T_ENTIER, adresseGlobale,
                                     -1);
                adresseGlobale += 4;
                if (AFFICHE_MIPS) {
                    printf("v%s:", n->nom);
                    printf("\t.space\t4\n");  // .space 4
                }
                break;
            case C_VARIABLE_LOCALE:
                ajouteIdentificateur(n->nom, contexte, T_ENTIER,
                                     adresseLocaleCourante, -1);
                adresseLocaleCourante += 4;
                break;
            case C_ARGUMENT:
                ajouteIdentificateur(n->nom, contexte, T_ENTIER,
                                     adresseArgumentCourant, -1);
                adresseArgumentCourant += 4;
                break;
        }
        return;
    }
    printf("%s \n", n->nom);
    erreur_1s("Erreur dans la table des symboles", "Variable déjà déclarée");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/

void table_tabDec(n_dec *n) {
    if (rechercheDeclarative(n->nom) == -1) {
        ajouteIdentificateur(n->nom, C_VARIABLE_GLOBALE, T_TABLEAU_ENTIER,
                             dico.sommet, n->u.tabDec_.taille);
        /* MIPS */
        if (AFFICHE_MIPS) {
            printf("v%s:", n->nom);
            printf("\t.space\t%d\n", n->u.tabDec_.taille * 4);  // .space *
        }
        return;
    }
    printf("%s \n", n->nom);
    erreur_1s("Erreur dans la table des symboles", "Tableau déja déclaré");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/

void table_var(n_var *n) {
    if (n->type == simple) {
        table_var_simple(n);
    } else if (n->type == indicee) {
        table_var_indicee(n);
    }
}

/*-------------------------------------------------------------------------
 * Nous testons dans cette fonction si la variable utilisée est hors contexte et
 * si elle est bien déclarée
 * ------------------------------------------------------------------------*/
void table_var_simple(n_var *n) {
    int i;
    if ((i = rechercheExecutable(n->nom)) != -1) {
        desc_identif *var = &dico.tab[i];
        if (var->type != T_ENTIER) {
            erreur_1s("Erreur dans la table des symboles",
                      "mauvais type de variable, variable simple attendue");
            exit(EXIT_FAILURE);
        }
        return;
    }
    erreur_1s("Erreur dans la table des symboles", "Variable non déclarée");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/
void table_var_indicee(n_var *n) {
    int i;
    if ((i = rechercheExecutable(n->nom)) != -1) {
        desc_identif *var = &dico.tab[i];
        if (var->type != T_TABLEAU_ENTIER) {
            printf("%s \n", n->nom);
            erreur_1s("Erreur dans la table des symboles",
                      "Tableau déclarée comme entier");
            exit(EXIT_FAILURE);
        }
        table_exp(n->u.indicee_.indice);
        return;
    }
    printf("%s \n", n->nom);
    erreur_1s("Erreur dans la table des symboles", "Tableau non déclaré");
    exit(EXIT_FAILURE);
}

/*-------------------------------------------------------------------------*/

int size_n_l_dec(n_l_dec *n) {
    if (n == NULL) return 0;
    return 1 + size_n_l_dec(n->queue);
}

int size_n_l_exp(n_l_exp *n) {
    if (n == NULL) return 0;
    return 1 + size_n_l_exp(n->queue);
}

void empile(char *reg) {
    printf("\tsubi\t$sp, $sp, 4\t# alloue un mot sur la pile\n");
    printf("\tsw\t%s, 0($sp)\t# copie reg vers sommet de pile\n", reg);
}

void depile(char *reg) {
    printf("\tlw\t%s, 0($sp)\t# copie sommet de pile vers reg\n", reg);
    printf("\taddi\t$sp, $sp, 4\t# desalloue un mot sur la pile\n");
}

int newTag() {
    ++cptInstr;
    return cptInstr;
}
