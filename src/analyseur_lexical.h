#ifndef __LIRE_UNITE__
#define __LIRE_UNITE__

#include "stdio.h"

int yylex(void);
void nom_token( int token, char *nom, char *valeur );
void test_yylex_internal( FILE *yyin );
int is_symbole_simple(char c);
int is_mot_clefs();
void affiche_xml_valeur(int token);

#endif

