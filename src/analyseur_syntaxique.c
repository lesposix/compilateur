#include "analyseur_syntaxique.h"
#include "analyseur_lexical.h"
#include "symboles.h"
#include "premiers.h"
#include "suivants.h"
#include "syntabs.h"
#include "affiche_arbre_abstrait.h"
#include "util.h"
#include <string.h>

#define DEBUG
// -DDEBUG

char *SYNTAX_ERROR_MSG = "Erreur de syntaxe";

int uniteCourante;
char nom[100];
char valeur[100];

void affiche_et_next() {
    affiche_xml_valeur(uniteCourante);
    nom_token(uniteCourante, nom, valeur);
    uniteCourante = yylex();
}

void analyser_syntaxe(FILE *yyin) {
    initialise_premiers();
    initialise_suivants();

    uniteCourante = yylex();
    n_prog *p = PG();
    affiche_n_prog(p);
}

n_prog *PG() {
    affiche_balise_ouvrante("programme", AFFICHE_XML);

    n_prog *$$ = NULL;
    n_l_dec *$1 = NULL;
    n_l_dec *$2 = NULL;

    if (est_premier(uniteCourante, _optDecVariables_) ||
            est_premier(uniteCourante, _listeDecFonctions_) ||
            est_suivant(uniteCourante, _programme_)) {
        $1 = ODV();
        $2 = LDF();

        $$ = cree_n_prog($1, $2);
        affiche_balise_fermante("programme", AFFICHE_XML);

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "\'entier\' ou \'un ientifiant de fonction\'"
            "ou rien si vous êtes un petit peu fatigué(e) ;).");

    exit(EXIT_FAILURE);
}

n_l_dec *ODV() {
    affiche_balise_ouvrante("optDecVariables", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_l_dec *$1 = NULL;

    if (est_premier(uniteCourante, _listeDecVariables_)) {
        $1 = LDV();
        if (uniteCourante == POINT_VIRGULE) {
            affiche_et_next();
            affiche_balise_fermante("optDecVariables", AFFICHE_XML);
            $$ = $1;

            return $$;
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "\';\' (le point virgule) ");
            exit(EXIT_FAILURE);
        }
    } else if (est_suivant(uniteCourante, _optDecVariables_)) {
        affiche_balise_fermante("optDecVariables", AFFICHE_XML);

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le mot clef \'entier\' "
            "ou rien si vous encore et toujours fatigué");
    exit(EXIT_FAILURE);
}

n_l_dec *LDV() {
    affiche_balise_ouvrante("listeDecVariables", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_dec *$1 = NULL;
    n_l_dec *$2 = NULL;

    if (est_premier(uniteCourante, _declarationVariable_)) {
        $1 = DV();
        $2 = LDVB($$);
        $$ = cree_n_l_dec($1, $2);
        affiche_balise_fermante("listeDecVariables", AFFICHE_XML);

        return $$;
    }

    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le mot clef \'entier\'");
    exit(EXIT_FAILURE);
}

n_l_dec *LDVB(n_l_dec *herite) {
    affiche_balise_ouvrante("listeDecVariablesBis", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_dec *$2 = NULL;
    n_l_dec *herite_fils = NULL;

    if (uniteCourante == VIRGULE) {
        affiche_et_next();
        $2 = DV();
        herite_fils = LDVB(herite);
        $$ = cree_n_l_dec($2, herite_fils);

        affiche_balise_fermante("listeDecVariablesBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _listeDecVariablesBis_)) {
        affiche_balise_fermante("listeDecVariablesBis", AFFICHE_XML);
        $$ = herite;
        return $$;
    }

    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le caractère \',\' ou rien");
    exit(EXIT_FAILURE);
}

n_dec *DV() {
    affiche_balise_ouvrante("declarationVariable", AFFICHE_XML);

    n_dec *$$ = NULL;
    n_exp *$2 = NULL;

    if (uniteCourante == ENTIER) {
        affiche_et_next();
        if (uniteCourante == ID_VAR) {
            affiche_et_next();
            char *nom_var = (char *)malloc(100 * sizeof(char));
            strcpy(nom_var, valeur);
            $2 = OTT();
            affiche_balise_fermante("declarationVariable", AFFICHE_XML);

            if ($2 != NULL)
                $$ = cree_n_dec_tab(nom_var, $2->u.entier);
            else
                $$ = cree_n_dec_var(nom_var);

            return $$;
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "un identifiant de variable\'id_var\'");
            exit(EXIT_FAILURE);
        }
    }

    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le mot clef \'entier\'");
    exit(EXIT_FAILURE);
}

n_exp *OTT() {
    affiche_balise_ouvrante("optTailleTableau", AFFICHE_XML);

    n_exp *$$ = NULL;

    if (uniteCourante == CROCHET_OUVRANT) {
        affiche_et_next();
        if (uniteCourante == NOMBRE) {
            affiche_et_next();
            $$ = cree_n_exp_entier(atoi(valeur));

            if (uniteCourante == CROCHET_FERMANT) {
                affiche_et_next();
                affiche_balise_fermante("optTailleTableau", AFFICHE_XML);

                return $$;
            } else {
                erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                        "le caractere \']\' (crochet fermant)");
                exit(EXIT_FAILURE);
            }
            affiche_balise_fermante("optTailleTableau", AFFICHE_XML);
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "un nombre \'nb\'");
            exit(EXIT_FAILURE);
        }
    } else if (est_suivant(uniteCourante, _optTailleTableau_)) {
        affiche_balise_fermante("optTailleTableau", AFFICHE_XML);

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le caractere \'[\' (crochet ouvrant) ou rien");
    exit(EXIT_FAILURE);
}

n_l_dec *LDF() {
    affiche_balise_ouvrante("listeDecFonctions", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_dec *$1 = NULL;
    n_l_dec *$2 = NULL;

    if (est_premier(uniteCourante, _declarationFonction_)) {
        $1 = DF();
        $2 = LDF();
        affiche_balise_fermante("listeDecFonctions", AFFICHE_XML);

        $$ = cree_n_l_dec($1, $2);

        return $$;
    } else if (est_suivant(uniteCourante, _listeDecFonctions_)) {
        affiche_balise_fermante("listeDecFonctions", AFFICHE_XML);

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "un identificateur de fonction ou rien");
    affiche_balise_fermante("listeDecFonctions", AFFICHE_XML);
    exit(EXIT_FAILURE);
}

n_dec *DF() {
    affiche_balise_ouvrante("declarationFonction", AFFICHE_XML);

    n_dec *$$ = NULL;
    n_l_dec *$2 = NULL;
    n_l_dec *$3 = NULL;
    n_instr *$4 = NULL;

    if (uniteCourante == ID_FCT) {
        affiche_et_next();
        char *nom_f = (char *)malloc(100 * sizeof(char));
        strcpy(nom_f, valeur);
        $2 = LP();
        $3 = ODV();
        $4 = IB();
        affiche_balise_fermante("declarationFonction", AFFICHE_XML);

        $$ = cree_n_dec_fonc(nom_f, $2, $3, $4);

        return $$;
    }
    erreur_1s("Erreur de syntaxe,", "lors de la déclaration de la fonction");
    exit(EXIT_FAILURE);
}

n_l_dec *LP() {
    affiche_balise_ouvrante("listeParam", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_l_dec *$2 = NULL;

    if (uniteCourante == PARENTHESE_OUVRANTE) {
        affiche_et_next();
        $2 = OLDV();
        if (uniteCourante == PARENTHESE_FERMANTE) {
            affiche_et_next();
            affiche_balise_fermante("listeParam", AFFICHE_XML);

            $$ = $2;

            return $$;
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "\')\' (la parenthèse fermante)");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "\'(\' (la parenthèse ouvrante)");
    exit(EXIT_FAILURE);
}

n_l_dec *OLDV() {
    affiche_balise_ouvrante("optListeDecVariables", AFFICHE_XML);

    n_l_dec *$$ = NULL;
    n_l_dec *$1 = NULL;

    if (est_premier(uniteCourante, _listeDecVariables_)) {
        $1 = LDV();
        affiche_balise_fermante("optListeDecVariables", AFFICHE_XML);

        $$ = $1;

        return $$;
    } else if (est_suivant(uniteCourante, _optListeDecVariables_)) {
        affiche_balise_fermante("optListeDecVariables", AFFICHE_XML);

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "le mot clef \'entier\'");
    exit(EXIT_FAILURE);
}

n_instr *I() {
    affiche_balise_ouvrante("instruction", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_instr *$1 = NULL;

    if (est_premier(uniteCourante, _instructionAffect_)) {
        $1 = IAFF();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionBloc_)) {
        $1 = IB();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionSi_)) {
        $1 = ISI();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionTantque_)) {
        $1 = ITQ();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionPour_)) {
        $1 = IPOUR();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionAppel_)) {
        $1 = IAPP();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionRetour_)) {
        $1 = IRET();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionEcriture_)) {
        $1 = IECR();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    } else if (est_premier(uniteCourante, _instructionVide_)) {
        $1 = IVIDE();
        affiche_balise_fermante("instruction", AFFICHE_XML);
        $$ = $1;

        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "un identifiant de variable, ou "
            "une accolade ouvrante \'}\' ou "
            "le mot clef \'si\' ou"
            "le mot clef \'tantque\' ou"
            "un identifiant de fonction ou"
            "le mot clef \'retour\' ou"
            "le mot clef \'ecrire\' ou "
            "un point virgule \';\'");
    exit(EXIT_FAILURE);
}

n_instr *IAFF() {
    affiche_balise_ouvrante("instructionAffect", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_var *$1 = NULL;
    n_exp *$3 = NULL;

    if (est_premier(uniteCourante, _var_)) {
        $1 = VAR();
        if (uniteCourante == EGAL) {
            affiche_et_next();
            $3 = EXP();
            if (uniteCourante == POINT_VIRGULE) {
                affiche_et_next();
                affiche_balise_fermante("instructionAffect", AFFICHE_XML);
                $$ = cree_n_instr_affect($1, $3);

                return $$;
            } else {
                erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                        "\';\' (le point virgule, le fameux ;) )");
                exit(EXIT_FAILURE);
            }
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "\'=\' (l\'opérateur égal)");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "un identifiant de variable");
    exit(EXIT_FAILURE);
}

n_instr *IB() {
    affiche_balise_ouvrante("instructionBloc", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_l_instr *$2 = NULL;

    if (uniteCourante == ACCOLADE_OUVRANTE) {
        affiche_et_next();
        $2 = LI();
        if (uniteCourante == ACCOLADE_FERMANTE) {
            affiche_et_next();
            affiche_balise_fermante("instructionBloc", AFFICHE_XML);
            $$ = cree_n_instr_bloc($2);

            return $$;
        } else {
            erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
                    "\'}\' une accolade fermante");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "\'{\' une accolade ouvrante");
    exit(EXIT_FAILURE);
}

n_l_instr *LI() {
    affiche_balise_ouvrante("listeInstructions", AFFICHE_XML);

    n_l_instr *$$ = NULL;
    n_instr *$1 = NULL;
    n_l_instr *$2 = NULL;

    if (est_premier(uniteCourante, _instruction_)) {
        $1 = I();
        $2 = LI();
        affiche_balise_fermante("listeInstructions", AFFICHE_XML);
        $$ = cree_n_l_instr($1, $2);

        return $$;
    } else if (est_suivant(uniteCourante, _listeInstructions_)) {
        affiche_balise_fermante("listeInstructions", AFFICHE_XML);
        return $$;
    }
    erreur_1s("Erreur de syntaxe, voila ce qui est attendu: ",
            "\'{\' un crochet ouvrant");
    exit(EXIT_FAILURE);
}

n_instr *ISI() {
    affiche_balise_ouvrante("instructionSi", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_exp *$2 = NULL;
    n_instr *$4 = NULL;
    n_instr *$5 = NULL;

    if (uniteCourante == SI) {
        affiche_et_next();
        $2 = EXP();
        if (uniteCourante == ALORS) {
            affiche_et_next();
            $4 = IB();
            $5 = OSINON();
            affiche_balise_fermante("instructionSi", AFFICHE_XML);

            $$ = cree_n_instr_si($2, $4, $5);

            return $$;
        } else {
            erreur_1s(SYNTAX_ERROR_MSG, "Mot clé \'alors\'");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s(SYNTAX_ERROR_MSG, "mot clé \'si\'");
    exit(EXIT_FAILURE);
}

n_instr *OSINON() {
    affiche_balise_ouvrante("optSinon", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_instr *$2 = NULL;

    if (uniteCourante == SINON) {
        affiche_et_next();
        $2 = IB();
        affiche_balise_fermante("optSinon", AFFICHE_XML);

        $$ = $2;

        return $$;
    } else if (est_suivant(uniteCourante, _optSinon_)) {
        affiche_balise_fermante("optSinon", AFFICHE_XML);
        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "sinon ou rien");
    exit(EXIT_FAILURE);
}

n_instr *ITQ() {
    affiche_balise_ouvrante("instructionTantque", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_exp *$2 = NULL;
    n_instr *$4 = NULL;

    if (uniteCourante == TANTQUE) {
        affiche_et_next();
        $2 = EXP();
        if (uniteCourante == FAIRE) {
            affiche_et_next();
            $4 = IB();
            affiche_balise_fermante("instructionTantque", AFFICHE_XML);

            $$ = cree_n_instr_tantque($2, $4);

            return $$;
        }
        erreur_1s(SYNTAX_ERROR_MSG, "missing faire");
        exit(EXIT_FAILURE);
    }
    erreur_1s(SYNTAX_ERROR_MSG, "instruction tantque");
    exit(EXIT_FAILURE);
}

n_instr *IPOUR() {
    affiche_balise_ouvrante("instructionPour", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_instr *$2 = NULL;
    n_exp *$3 = NULL;
    n_instr *$5 = NULL;
    n_instr *$7 = NULL;

    if (uniteCourante == POUR) {
        affiche_et_next();
        $2 = IAFF();
        $3 = EXP();
        if (uniteCourante == POINT_VIRGULE) {
            affiche_et_next();
            $5 = IAFF();
            if (uniteCourante == FAIRE) {
                affiche_et_next();
                $7 = IB();
                affiche_balise_fermante("instructionPour", AFFICHE_XML);

                $$ = cree_n_instr_pour($2, $3, $5, $7);

                return $$;
            }
            erreur_1s(SYNTAX_ERROR_MSG, "missing faire");
            exit(EXIT_FAILURE);
        }
        erreur_1s(SYNTAX_ERROR_MSG, "Mising ;");
        exit(EXIT_FAILURE);
    }
    printf("uniteCourante %d", uniteCourante);
    erreur_1s(SYNTAX_ERROR_MSG, "instruction Pour");
    exit(EXIT_FAILURE);
}

n_instr *IAPP() {
    affiche_balise_ouvrante("instructionAppel", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_appel *$1 = NULL;

    if (est_premier(uniteCourante, _appelFct_)) {
        $1 = APPF();
        if (uniteCourante == POINT_VIRGULE) {
            affiche_et_next();
            affiche_balise_fermante("instructionAppel", AFFICHE_XML);

            $$ = cree_n_instr_appel($1);

            return $$;
        } else {
            erreur_1s(SYNTAX_ERROR_MSG, "\';\' (point virgule manquant)");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s(SYNTAX_ERROR_MSG, "un identifiant de fonction");
    exit(EXIT_FAILURE);
}

n_instr *IRET() {
    affiche_balise_ouvrante("instructionRetour", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_exp *$2 = NULL;

    if (uniteCourante == RETOUR) {
        affiche_et_next();
        $2 = EXP();

        if (uniteCourante == POINT_VIRGULE) {
            affiche_et_next();
            affiche_balise_fermante("instructionRetour", AFFICHE_XML);

            $$ = cree_n_instr_retour($2);

            return $$;
        } else {
            erreur_1s(SYNTAX_ERROR_MSG, "\';\' (point virgule manquant)");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s(SYNTAX_ERROR_MSG, "le mot clef \'retour\'");
    exit(EXIT_FAILURE);
}

n_instr *IECR() {
    affiche_balise_ouvrante("instructionEcriture", AFFICHE_XML);

    n_instr *$$ = NULL;
    n_exp *$3 = NULL;

    if (uniteCourante == ECRIRE) {
        affiche_et_next();

        if (uniteCourante == PARENTHESE_OUVRANTE) {
            affiche_et_next();
            $3 = EXP();

            if (uniteCourante == PARENTHESE_FERMANTE) {
                affiche_et_next();

                if (uniteCourante == POINT_VIRGULE) {
                    affiche_et_next();
                    affiche_balise_fermante("instructionEcriture", AFFICHE_XML);

                    $$ = cree_n_instr_ecrire($3);

                    return $$;
                } else {
                    erreur_1s(SYNTAX_ERROR_MSG, "\';\' (le point virgule)");
                    exit(EXIT_FAILURE);
                }
            } else {
                erreur_1s(SYNTAX_ERROR_MSG, "\')\' (la parenthèse fermante)");
                exit(EXIT_FAILURE);
            }
        } else {
            erreur_1s(SYNTAX_ERROR_MSG, "\'(\' (la parenthèse ouvrante)");
            exit(EXIT_FAILURE);
        }
    }
    erreur_1s(SYNTAX_ERROR_MSG, "le mot clef: \'ecrire\'");
    exit(EXIT_FAILURE);
}

n_instr *IVIDE() {
    affiche_balise_ouvrante("instructionVide", AFFICHE_XML);

    n_instr *$$ = NULL;

    if (uniteCourante == POINT_VIRGULE) {
        affiche_et_next();
        affiche_balise_fermante("instructionVide", AFFICHE_XML);

        $$ = cree_n_instr_vide();

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "\';\' (le point virgule)");
    exit(EXIT_FAILURE);
}

n_exp *COND() {
    affiche_balise_ouvrante("condition", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_exp *$2 = NULL;

    if (est_premier(uniteCourante, _conjonction_)) {
        $1 = CONJ();
        $$ = $1;
        $2 = CONDB($$);
        $$ = $2;
        affiche_balise_fermante("condition", AFFICHE_XML);

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG,
            "\'!\' (le non) "
            "le mot clef \'lire\' "
            "un nombre "
            "\'(\' (la parenthèse ouvrante) "
            "un identifiant de variable "
            "ou un identifiant de fonction | expression");
    exit(EXIT_FAILURE);
}

n_exp *CONDB(n_exp *herite) {
    affiche_balise_ouvrante("conditionBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == OU) {
        affiche_et_next();
        $2 = CONJ();
        herite_fils = CONDB(herite);
        $$ = cree_n_exp_op(ou, herite_fils, $2, NULL);
        affiche_balise_fermante("conditionBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _conditionBis_)) {
        affiche_balise_fermante("conditionBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "\'|\' (l\'opérateur OU)");
    exit(EXIT_FAILURE);
}

n_exp *CONJ() {
    affiche_balise_ouvrante("conjonction", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_exp *$2 = NULL;

    if (est_premier(uniteCourante, _negation_)) {
        $1 = NEG($$);
        $$ = $1;
        $2 = CONJB($$);
        $$ = $2;
        affiche_balise_fermante("conjonction", AFFICHE_XML);

        return $$;
    }
    printf("%d\n",uniteCourante);
    erreur_1s(SYNTAX_ERROR_MSG,
            "\'!\' (le non) "
            "le mot clef \'lire\' "
            "un nombre "
            "\'(\' (la parenthèse ouvrante) "
            "un identifiant de variable "
            "ou un identifiant de fonction | conjonction");
    exit(EXIT_FAILURE);
}

n_exp *CONJB(n_exp *herite) {
    affiche_balise_ouvrante("conjonctionBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == ET) {
        affiche_et_next();
        $2 = NEG(herite);
        herite_fils = CONJB(herite);
        $$ = cree_n_exp_op(et, herite_fils, $2, NULL);

        affiche_balise_fermante("conjonctionBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _conjonctionBis_)) {
        affiche_balise_fermante("conjonctionBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "\'&\' (l\'opérateur ET)");
    exit(EXIT_FAILURE);
}

n_exp *NEG(n_exp *herite) {
    affiche_balise_ouvrante("negation", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;

    if (uniteCourante == NON) {
        affiche_et_next();
        $2 = COMP();
		$$ = cree_n_exp_op(non, $2, NULL, NULL);
        affiche_balise_fermante("negation", AFFICHE_XML);

        return $$;
    } else if (est_premier(uniteCourante, _comparaison_)) {
        $2 = COMP();
        $$ = $2;
        affiche_balise_fermante("negation", AFFICHE_XML);

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "\'!\' (l\'opérateur NON)");
    exit(EXIT_FAILURE);
}

n_exp *COMP() {
    affiche_balise_ouvrante("comparaison", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_exp *$2 = NULL;

    if (est_premier(uniteCourante, _expArith_)) {
        $1 = E();
        $$ = $1;
        $2 = COMPB($$);
        $$ = $2;
        affiche_balise_fermante("comparaison", AFFICHE_XML);

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG,
            "le mot clef \'lire\'"
            "un nombre"
            "\'(\' la parenthèse ouvrante)"
            "un identifiant de variable"
            "ou un identifiant de fonction");
    exit(EXIT_FAILURE);
}

n_exp *COMPB(n_exp *herite) {
    affiche_balise_ouvrante("comparaisonBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == EGAL) {
        affiche_et_next();
        $2 = E();
        herite_fils = COMPB(herite);
        $$ = cree_n_exp_op(egal, herite_fils, $2, NULL);
        affiche_balise_fermante("comparaisonBis", AFFICHE_XML);

        return $$;
    } else if (uniteCourante == INFERIEUR) {
        affiche_et_next();
        $2 = E();
        herite_fils = COMPB(herite);
        $$ = cree_n_exp_op(inf, herite_fils, $2, NULL);
        affiche_balise_fermante("comparaisonBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _comparaisonBis_)) {
        affiche_balise_fermante("comparaisonBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "=, < ou rien");
    exit(EXIT_FAILURE);
}

n_exp *E() {
    affiche_balise_ouvrante("expArith", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_exp *$2 = NULL;

    if (est_premier(uniteCourante, _terme_)) {
        $1 = T();
        $$ = $1;
        $2 = EB($$);
        $$ = $2;
        affiche_balise_fermante("expArith", AFFICHE_XML);

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "expression arithmetique attendue");
    exit(EXIT_FAILURE);
}

n_exp *EB(n_exp *herite) {
    affiche_balise_ouvrante("expArithBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == PLUS) {
        affiche_et_next();
        $2 = T();
        herite_fils=cree_n_exp_op(plus, herite, $2, NULL);
        $$ = EB(herite_fils);
        affiche_balise_fermante("expArithBis", AFFICHE_XML);

        return $$;
    } else if (uniteCourante == MOINS) {
        affiche_et_next();
        $2 = T();
        herite_fils=cree_n_exp_op(moins, herite, $2, NULL);
        $$ = EB(herite_fils);
        affiche_balise_fermante("expArithBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _expArithBis_)) {
        affiche_balise_fermante("expArithBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "expression arithmetique attendue");
    exit(EXIT_FAILURE);
}

n_exp *T() {
    affiche_balise_ouvrante("terme", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_exp *$2 = NULL;

    if (est_premier(uniteCourante, _facteur_)) {
        $1 = F();
        $$ = $1;
        $2 = TB($$);
        $$ = $2;
        affiche_balise_fermante("terme", AFFICHE_XML);

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "terme attendu");
    exit(EXIT_FAILURE);
}

n_exp *TB(n_exp *herite) {
    affiche_balise_ouvrante("termeBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == FOIS) {
        affiche_et_next();
        $2 = F();
        herite_fils=cree_n_exp_op(fois, herite, $2, NULL);
        $$ = TB(herite_fils);
        affiche_balise_fermante("termeBis", AFFICHE_XML);

        return $$;
    } else if (uniteCourante == DIVISE) {
        affiche_et_next();
        $2 = F();
        herite_fils=cree_n_exp_op(divise, herite, $2, NULL);
        $$ = TB(herite_fils);
        affiche_balise_fermante("termeBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _termeBis_)) {
        affiche_balise_fermante("termeBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "terme attendu");
    exit(EXIT_FAILURE);
}

n_exp *F() {
    affiche_balise_ouvrante("facteur", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_appel *$app = NULL;
    n_var *$var = NULL;

    if (uniteCourante == PARENTHESE_OUVRANTE) {
        affiche_et_next();
        $1 = EXP();
        if (uniteCourante == PARENTHESE_FERMANTE) {
            affiche_et_next();
            affiche_balise_fermante("facteur", AFFICHE_XML);
            $$ = $1;

            return $$;
        }
        erreur_1s(SYNTAX_ERROR_MSG, ") manquante");
        exit(EXIT_FAILURE);
    } else if (uniteCourante == NOMBRE) {
        affiche_et_next();
        affiche_balise_fermante("facteur", AFFICHE_XML);
        $$ = cree_n_exp_entier(atoi(valeur));

        return $$;
    } else if (uniteCourante == LIRE) {
        affiche_et_next();
        if (uniteCourante == PARENTHESE_OUVRANTE) {
            affiche_et_next();
            if (uniteCourante == PARENTHESE_FERMANTE) {
                affiche_et_next();
                affiche_balise_fermante("facteur", AFFICHE_XML);
                $$ = cree_n_exp_lire();

                return $$;
            }
            erreur_1s(SYNTAX_ERROR_MSG, ") manquante pour fonction lire()");
            exit(EXIT_FAILURE);
        }
        erreur_1s(SYNTAX_ERROR_MSG, "( manquante pour fonction lire()");
        exit(EXIT_FAILURE);
    } else if (est_premier(uniteCourante, _appelFct_)) {
        $app = APPF();
        affiche_balise_fermante("facteur", AFFICHE_XML);
        $$ = cree_n_exp_appel($app);

        return $$;
    } else if (est_premier(uniteCourante, _var_)) {
        $var = VAR();
        affiche_balise_fermante("facteur", AFFICHE_XML);
        $$ = cree_n_exp_var($var);

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "facteur attendu");
    exit(EXIT_FAILURE);
}

n_var *VAR() {
    affiche_balise_ouvrante("var", AFFICHE_XML);

    n_var *$$ = NULL;
    n_exp *$2 = NULL;

    if (uniteCourante == ID_VAR) {
        affiche_et_next();
        char *nom_var = (char *)malloc(100 * sizeof(char));
        strcpy(nom_var, valeur);
        $2 = OIND();
        affiche_balise_fermante("var", AFFICHE_XML);
        if ($2 != NULL)
            $$ = cree_n_var_indicee(nom_var, $2);
        else
            $$ = cree_n_var_simple(nom_var);

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "variable attendue");
    exit(EXIT_FAILURE);
}

n_exp *OIND() {
    affiche_balise_ouvrante("optIndice", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;

    if (uniteCourante == CROCHET_OUVRANT) {
        affiche_et_next();
        $2 = EXP();
        if (uniteCourante == CROCHET_FERMANT) {
            affiche_et_next();
            affiche_balise_fermante("optIndice", AFFICHE_XML);
            $$ = $2;

            return $$;
        }
        erreur_1s(SYNTAX_ERROR_MSG, "] manquant");
        exit(EXIT_FAILURE);
    } else if (est_suivant(uniteCourante, _optIndice_)) {
        affiche_balise_fermante("optIndice", AFFICHE_XML);

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "[] ou rien");
    exit(EXIT_FAILURE);
}

n_appel *APPF() {
    affiche_balise_ouvrante("appelFct", AFFICHE_XML);

    n_appel *$$ = NULL;
    n_l_exp *$3 = NULL;

    if (uniteCourante == ID_FCT) {
        affiche_et_next();
        char *nom_f = (char *)malloc(100 * sizeof(char));
        strcpy(nom_f, valeur);
        if (uniteCourante == PARENTHESE_OUVRANTE) {
            affiche_et_next();
            $3 = LEXP();
            if (uniteCourante == PARENTHESE_FERMANTE) {
                affiche_et_next();
                affiche_balise_fermante("appelFct", AFFICHE_XML);
                $$ = cree_n_appel(nom_f, $3);

                return $$;
            }
            erreur_1s(SYNTAX_ERROR_MSG, ") manquante sur appel fonction");
            exit(EXIT_FAILURE);
        }
        erreur_1s(SYNTAX_ERROR_MSG, "() parenthese manquante sur appel fonction");
        exit(EXIT_FAILURE);
    }

    erreur_1s(SYNTAX_ERROR_MSG, "appel fonction");
    exit(EXIT_FAILURE);
}

n_l_exp *LEXP() {
    affiche_balise_ouvrante("listeExpressions", AFFICHE_XML);

    n_l_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_l_exp *$2 = NULL;

    if (est_premier(uniteCourante, _expression_)) {
        $1 = EXP();
        $2 = LEXPB($$);
        $$ = cree_n_l_exp($1, $2);
        affiche_balise_fermante("listeExpressions", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _listeExpressions_)) {
        affiche_balise_fermante("listeExpressions", AFFICHE_XML);
        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "liste expression");
    exit(EXIT_FAILURE);
}

n_l_exp *LEXPB(n_l_exp *herite) {
    affiche_balise_ouvrante("listeExpressionsBis", AFFICHE_XML);

    n_l_exp *$$ = NULL;
    n_exp *$1 = NULL;
    n_l_exp *herite_fils = NULL;

    if (uniteCourante == VIRGULE) {
        affiche_et_next();
        $1 = EXP();
        herite_fils = LEXPB(herite);
        $$ = cree_n_l_exp($1, herite_fils);
        affiche_balise_fermante("listeExpressionsBis", AFFICHE_XML);

        return $$;
    } else if (est_suivant(uniteCourante, _listeExpressionsBis_)) {
        affiche_balise_fermante("listeExpressionsBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }

    erreur_1s(SYNTAX_ERROR_MSG, "liste expression bis");
    exit(EXIT_FAILURE);
}

n_exp *EXP(){
    affiche_balise_ouvrante("expression", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$1 = NULL;

    if (est_premier(uniteCourante, _condition_)) {
        $1 = COND();
        $$ = EXPB($1);
        affiche_balise_fermante("expression", AFFICHE_XML);

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG,
            "\'!\' (le non) "
            "le mot clef \'lire\' "
            "un nombre "
            "\'(\' (la parenthèse ouvrante) "
            "un identifiant de variable "
            "ou un identifiant de fonction | expression");
    exit(EXIT_FAILURE);
}

n_exp *EXPB(n_exp *herite){
    affiche_balise_ouvrante("expressionBis", AFFICHE_XML);

    n_exp *$$ = NULL;
    n_exp *$2 = NULL;
    n_exp *$3 = NULL;
    n_exp *herite_fils = NULL;

    if (uniteCourante == INTERROGATION) {
        affiche_et_next();
        $2 = EXP();
        if(uniteCourante == DEUXPOINTS) {
            affiche_et_next();
            $3 = EXP();

            herite_fils = EXPB(herite);
            $$ = cree_n_exp_op(tern, herite_fils, $2, $3);
            affiche_balise_fermante("expressionBis", AFFICHE_XML);

            return $$;
        }
        erreur_1s(SYNTAX_ERROR_MSG, "attendue (l\'opéperateur :)");
        exit(EXIT_FAILURE);

    } else if (est_suivant(uniteCourante, _expressionBis_)) {
        affiche_balise_fermante("expressionBis", AFFICHE_XML);
        $$ = herite;

        return $$;
    }
    erreur_1s(SYNTAX_ERROR_MSG, "attendue (l\'opéperateur ?)");
    exit(EXIT_FAILURE);
}
